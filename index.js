const { Readable } = require("stream");
const AbortController = require("abort-controller");
const cheerio = require("cheerio");
const config = require("./config");
const csvParse = require("csv-parse");
const fetch = require("node-fetch");
const fs = require("fs");
const pLimit = require("p-limit");

const main = async (index) => {
  console.log(`\n${index}`);
  const indexConstituents = await fetchIndexConstituents(
    config.indexConstituentsUrls[index]
  );
  console.log(`Fetched ${indexConstituents.length} scrip codes.`);

  const limitProfitFetches = pLimit(Infinity);
  const limitMarketCapFetches = pLimit(Infinity);

  const getTtmNetProfitPromises = indexConstituents.map((scripCode) =>
    limitProfitFetches(() => getTtmNetProfit(scripCode))
  );
  const getTotalAndFreeFloatMarketCapPromises = indexConstituents.map(
    (scripCode) =>
      limitMarketCapFetches(() => getTotalAndFreeFloatMarketCap(scripCode))
  );

  const ttmNetProfits = await Promise.all(getTtmNetProfitPromises);
  const totalAndFreeFloatMarketCaps = await Promise.all(
    getTotalAndFreeFloatMarketCapPromises
  );
  console.log(
    `Fetched market data for ${indexConstituents.length} scrip codes.`
  );

  let totalFreeFloatMarketCapInCrores = 0;
  let totalFreeFloatAdjTtmNetProfitInCrores = 0;
  const excluded = [];
  const scripDetails = indexConstituents.map((scripCode, i) => {
    const freeFloatMarketCapInCrores =
      totalAndFreeFloatMarketCaps[i].freeFloatMarketCapInLakhs / 100;
    const totalMarketCapInCrores =
      totalAndFreeFloatMarketCaps[i].totalMarketCapInLakhs / 100;
    const freeFloatRatio = freeFloatMarketCapInCrores / totalMarketCapInCrores;

    const ttmNetProfitInCrores = ttmNetProfits[i];
    const freeFloatAdjTtmNetProfitInCrores =
      freeFloatRatio * ttmNetProfitInCrores;

    const pe = totalMarketCapInCrores / ttmNetProfitInCrores;

    const shouldExclude = config.SCRIPS_TO_EXCLUDE.has(scripCode);
    if (shouldExclude || (pe < 0 && Math.abs(pe) < config.MIN_NEGATIVE_PE)) {
      excluded.push(scripCode);
    } else {
      totalFreeFloatMarketCapInCrores += freeFloatMarketCapInCrores;
      totalFreeFloatAdjTtmNetProfitInCrores += freeFloatAdjTtmNetProfitInCrores;
    }

    return {
      scripCode,
      freeFloatMarketCapInCrores,
      freeFloatAdjTtmNetProfitInCrores,
      freeFloatRatio,
      totalMarketCapInCrores,
      ttmNetProfitInCrores,
      pe,
    };
  });

  scripDetails.map((scripDetail) => {
    scripDetail.weightageInIndex =
      (scripDetail.freeFloatMarketCapInCrores /
        totalFreeFloatMarketCapInCrores) *
      100;
    scripDetail.profitWeightageInIndex =
      (scripDetail.freeFloatAdjTtmNetProfitInCrores /
        totalFreeFloatAdjTtmNetProfitInCrores) *
      100;
  });

  const pe =
    totalFreeFloatMarketCapInCrores / totalFreeFloatAdjTtmNetProfitInCrores;
  scripDetails.sort(
    (a, b) =>
      a.freeFloatAdjTtmNetProfitInCrores - b.freeFloatAdjTtmNetProfitInCrores
  );

  Readable.from(
    JSON.stringify(
      {
        pe,
        totalFreeFloatMarketCapInCrores,
        totalFreeFloatAdjTtmNetProfitInCrores,
        excluded,
        scripDetails,
      },
      null,
      4
    )
  ).pipe(fs.createWriteStream(`./${index}.json`));
};

const fetchIndexConstituents = async (url) => {
  const response = await fetch_retry(url, {
    credentials: "omit",
    headers: {
      "User-Agent":
        "Opera/9.80 (Macintosh; Intel Mac OS X; U; en) Presto/2.2.15 Version/10.00",
      Accept: "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
      "Accept-Language": "en-US,en;q=0.5",
      "Upgrade-Insecure-Requests": "1",
      Connection: "keep-alive",
    },
    method: "GET",
    mode: "cors",
  });
  return processCsvRecords(await response.text());
};

const fetch_retry = async (url, options, n = 5) => {
  for (let i = 0; i < n; i++) {
    const controller = new AbortController();
    const timeout = setTimeout(() => {
      controller.abort();
    }, config.TIMEOUT_IN_MILLISECONDS);
    try {
      return await fetch(url, { ...options, signal: controller.signal });
    } catch (err) {
      const isLastAttempt = i + 1 === n;
      if (isLastAttempt) throw err;
    } finally {
      clearTimeout(timeout);
    }
  }
};

const processCsvRecords = (csvString) => {
  return new Promise((resolve, reject) => {
    csvParse(
      csvString,
      {
        columns: true,
      },
      (err, output) => {
        if (err) {
          reject(err);
        }

        resolve(output.map((data) => data.Symbol));
      }
    );
  });
};

const getTtmNetProfit = async (scripCode) => {
  const searchUrl = new URL("https://www.screener.in/api/company/search/");
  searchUrl.searchParams.append("q", scripCode);
  const searchQueryResponse = await fetch_retry(searchUrl);
  const searchQueryJson = await searchQueryResponse.json();

  let url;
  for (let searchQuery of searchQueryJson) {
    if (searchQuery.url.includes(`/${scripCode}/`)) {
      url = searchQuery.url;
      break;
    }
  }

  const response = await fetch_retry(`https://www.screener.in${url}`);
  const $ = cheerio.load(await response.text());

  return parseFloat(
    $(":contains(Net Profit)")
      .last()
      .parent()
      .children()
      .last()
      .text()
      .replace(/,/g, "")
  );
};

const getTotalAndFreeFloatMarketCap = async (scripCode) => {
  const nseGetQuoteUrl = new URL("https://www.nseindia.com/api/quote-equity");
  nseGetQuoteUrl.searchParams.append("symbol", scripCode);
  nseGetQuoteUrl.searchParams.append("section", "trade_info");
  let nseJsonResponse;
  try {
    nseJsonResponse = await callNseForScripCode(nseGetQuoteUrl);
  } catch (error) {
    console.log(
      `Error occured when fetching TotalAndFreeFloatMarketCap for ${scripCode}\n`,
      error
    );
    console.log(`Retrying again for ${scripCode}`);
    nseJsonResponse = await callNseForScripCode(nseGetQuoteUrl);
    console.log(`Sucessfully fetched for ${scripCode}`);
  }

  const tradeInfo = nseJsonResponse.marketDeptOrderBook.tradeInfo;
  return {
    totalMarketCapInLakhs: tradeInfo.totalMarketCap,
    freeFloatMarketCapInLakhs: tradeInfo.ffmc,
  };
};

const callNseForScripCode = async (nseGetQuoteUrl) => {
  return await (
    await fetch_retry(nseGetQuoteUrl, {
      headers: {
        authority: "www.nseindia.com",
        "user-agent":
          "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36",
        accept: "*/*",
        "sec-fetch-site": "same-origin",
        "sec-fetch-mode": "cors",
        "sec-fetch-dest": "empty",
        referer: "https://www.nseindia.com/get-quotes/equity?symbol=RELIANCE",
        "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
        cookie:
          'ak_bmsc=3083A56A25DA75BC9EE8ACFD2A6A715A17CB3F16D35E000086A42460D344AB41~plpFpETwyq5tkzzf9/vRPbAGuB0nXLCBRbH3RlrcV3okz5BnLftwPLusrSXVpzQnCh1RE/tJRUFTHHX1fVVfMiSkQgJ5yWeC51JZnfXrheieo+r8WQt9cP0FwwRVYXzLvlx80rgRZOKQW3BFQTeOKTnzwpJRB8/TKuPUEHRHwX2z5IQFJ3cYx5KaRoi7OtXPBafFVYKm1H6yAMrqeQwC7H412HlCrxN6SAgj+y/veVM+k=; nsit=cEDySaKd1-udjZloUZswzLib; AKA_A2=A; bm_mi=00C91F7E52A0D16ED58C062E3261DE5F~VENhH6btzYw4aPEDJiX80RXcgri+OHuOVgDEL8ccqzsGqJmPFQBbD4BRrwj9dBHDUelZV3I98BtKksSF/oVQ9IlqNmCJC2gIsuNiwvfoSsMOjPsgarbEC8yuwA4bnwH5wZuEmiynW4eGoR+h09IonUZ4EEIehHaboor8/Mu5DUrVW6eVNvzD9TomNbDg1bwIXiClc1prdKxosbwBXyBPXwvUAAXh6PBKaWF9Pd/evPVMVRfnlQGdIxbWRe4H7t2y; nseQuoteSymbols=[{"symbol":"RELIANCE","identifier":null,"type":"equity"}]; nseappid=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJhcGkubnNlIiwiYXVkIjoiYXBpLm5zZSIsImlhdCI6MTYxMzAxNDE1NSwiZXhwIjoxNjEzMDE3NzU1fQ.No6SUnV-p2_cVE9_PQavx9NACw4xLhuuip0On1oJSQ8; bm_sv=6E9326D4901F8866DBCC57893231DFC9~PeIjYis/wePv4Foa7eOyfFaN72L3MHkst6vl6/oTMAXOxGZd2B/0SxSVloDvW86M/y64oXpEiBb3/r+4wcgz7KtNG9s4ropKBa/AlV5nXLxK4lQGRa0u71D8KuciiVGfj7uA+UqFAjhDWz+lFQ1KZ5ZlbbOyjRyMoJ4vexcAIns=',
      },
    })
  ).json();
};

const mainHelper = async (indices) => {
  for (let index of indices) {
    await main(index);
  }
};

mainHelper([config.NIFTY_50, config.NIFTY_NEXT_50, config.NIFTY_MIDCAP_150]);

const indices = {
  NIFTY_50: "NIFTY_50",
  NIFTY_NEXT_50: "NIFTY_NEXT_50",
  NIFTY_MIDCAP_150: "NIFTY_MIDCAP_150",
  NIFTY_SMALLCAP_250: "NIFTY_SMALLCAP_250",
};

const config = {
  indexConstituentsUrls: {
    [indices.NIFTY_50]:
      "https://www.niftyindices.com/IndexConstituent/ind_nifty50list.csv",
    [indices.NIFTY_NEXT_50]:
      "https://www.niftyindices.com/IndexConstituent/ind_niftynext50list.csv",
    [indices.NIFTY_MIDCAP_150]:
      "https://www.niftyindices.com/IndexConstituent/ind_niftymidcap150list.csv",
    [indices.NIFTY_SMALLCAP_250]:
      "https://www.niftyindices.com/IndexConstituent/ind_niftysmallcap250list.csv",
  },
};

// Stocks with small negative P/E distort the index P/E as they will have huge losses.
// Use the MIN_NEGATIVE_PE to remove such stocks, enter small values like 3 remove big loss makers.
// Enter larger values to remove even small loss makers.
// Enter 0 to disable removing loss makers
const MIN_NEGATIVE_PE = 3;

module.exports = {
  ...config,
  ...indices,
  SCRIPS_TO_EXCLUDE: new Set(["BHARTIARTL", "IDEA"]),
  TIMEOUT_IN_MILLISECONDS: 15000,
  MIN_NEGATIVE_PE,
};
